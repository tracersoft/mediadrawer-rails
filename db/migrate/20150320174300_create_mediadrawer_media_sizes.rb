class CreateMediadrawerMediaSizes < ActiveRecord::Migration
  def change
    create_table :mediadrawer_media_sizes do |t|
      t.string :name
      t.string :size
      t.string :url
      t.references :mediadrawer_media, null:false

      t.timestamps
    end
  end
end
