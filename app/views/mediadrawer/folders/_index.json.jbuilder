json.array!(item.children.order('name')) do |child|
  json.partial! 'item', item: child
end