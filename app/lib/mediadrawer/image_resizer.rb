module Mediadrawer
  class ImageResizer
    def self.resize(file, sizes)
      sizes.each do |size_name, size|
        size = size.split 'x'
        if size.count == 2
          img = Magick::Image.read(file.path).first
          img.resize_to_fill!(size[0].to_i, size[1].to_i)
          img.write file.path
          yield(file, size_name, size)
        end
      end
    end
  end
end
