  class ContainerObj
    constructor: (@json, @container)->
      @node = @asJquery()
      @bindEvents()

    id: ->
      @json.id

    getNode: ->
      @node

    bindEvents: ->
      $(@node).click =>
        @container.setActive(this)
        @onClick()
        false
      if @container.mediadrawer.permissions.delete
        $(@node).on 'onmouseover mouseover', =>
          @onMouseOver()

        $(@node).on 'onmouseout mouseout', =>
          @onMouseOut()

        $(@node).find('a[data-remove]').click =>
          @delete()
      else
        $(@node).find('a[data-remove]').remove()

    onMouseOver: ->
      $(@node).find('a[data-remove]').show();

    onMouseOut: ->
      $(@node).find('a[data-remove]').hide();

    onClick: ->

    delete: ->
      if confirm @confirmMessage()
        $.ajax
          url: @json.resource_url,
          method: 'DELETE'
        $(@node).remove()

    confirmMessage: ->
      'Deseja realmente excluir?'

    asJquery: ->
      $(@asHTML())

    asHTML: ->
      HandlebarsTemplates[@template_type](@json)


  class Mediadrawer.Image extends ContainerObj
    template_type: 'img'

    confirmMessage: ->
      $('#mediadrawer .info-panel')
        .html('')
      $('#mediadrawer #md-files').removeClass('open-panel')
      'Deseja realmente excluir esta imagem?'

    onClick: ->
      $panel = $(HandlebarsTemplates["#{@template_type}-panel"](@json))

      $panel.on 'click', '[data-btn-update]', (e)=>
        @updateSubtitle($panel)

      $panel.on 'change', '.alt', (e)=>
        @updateSubtitle($panel)

      $('#mediadrawer .info-panel')
        .html($panel)
      $('#mediadrawer #md-files').addClass('open-panel')

    updateSubtitle: ($panel)->
      subtitle = $panel.find('.alt').val()
      btnUpdate = $panel.find('[data-btn-update]')
      btnUpdate.prop('disabled',true)

      status = btnUpdate.find('i');
      status.removeClass('fa-save').addClass('fa-spinner fa-spin');

      json = @json

      $.ajax
        url: @json.resource_url,
        data: { alt: subtitle },
        method: 'PATCH',
        success: ->
          json.alt = subtitle
          status.addClass('fa-save').removeClass('fa-spinner fa-spin');
          btnUpdate.prop('disabled',false)

  class Mediadrawer.File extends ContainerObj
    template_type: 'file'

    confirmMessage: ->
      'Deseja realmente excluir este arquivo?'

    onClick: ->
      $panel = $(HandlebarsTemplates["#{@template_type}-panel"](@json))
      $('#mediadrawer .info-panel')
        .html($panel)
      $('#mediadrawer #md-files').addClass('open-panel')


  class Mediadrawer.Folder extends ContainerObj
    path: ->
      @json.path

    confirmMessage: ->
      'Excluindo esta pasta todos os arquivos contidos serão excluidos. Deseja realmente excluir esta pasta?'

    delete: ->
      if confirm @confirmMessage()
        $('[data-folder-menu] li:first-child').click();
        $.ajax
          url: @json.resource_url,
          method: 'DELETE'
        $(@node).remove()

    onClick: ->
      @container.mediadrawer.mediaContainer.load()

    template_type: 'folder'

  class Mediadrawer.RootFolder extends Mediadrawer.Folder
    constructor: (@container)->
      @json =
        path: '',
        name: 'Home'
      super(@json, @container)


