module Mediadrawer
  class Media < ActiveRecord::Base
    include Magick
    belongs_to :folder, touch: true
    has_many :media_sizes, :class_name=> 'MediaSizes', foreign_key: 'mediadrawer_media_id', dependent: :destroy
    after_initialize :set_defaults

    after_destroy :delete_file

    DEFAULT_SIZE = 'original'

    def delete_file
      s3 = S3.new
      unless is_image?
        s3.delete path
      else
        s3.delete self.path(DEFAULT_SIZE)
        Mediadrawer.config['sizes'].each do |size_name, size|
          s3.delete self.path(size_name)
        end
      end
    end

    def type
      unless is_image?
        'file'
      else
        'image'
      end
    end

    def is_image?
      mime_type =~ /image/
    end

    def set_defaults
      self.folder ||= Folder.root
    end

    def name=(v)
      v = v.to_s
      extension = v.split('.').last
      name = v.split('.')[0...-1].join '.'
      write_attribute :name, "#{name.parameterize}.#{extension}"
    end

    def path(size=DEFAULT_SIZE)
      file_path_part = size ? "#{size}/#{name}" : name
      if self.folder.path.empty?
        file_path_part
      else
        "#{self.folder.path}/#{file_path_part}"
      end
    end

    def url
      url_for
    end

    def url_for(size=DEFAULT_SIZE)
      cache_key = "#{self.id}:#{self.updated_at}:#{size}:#{Mediadrawer.config[:asset_host]}"
      ::Rails.cache.fetch cache_key do
        remote_path = self.path(size)

        if Mediadrawer.config['asset_host']
          "#{Mediadrawer.config['asset_host']}/#{remote_path}"
        else
          s3 = S3.new
          s3[remote_path].public_url.to_s
        end
      end
    end

    def upload(content, sizes=[])
      unless content.kind_of? Tempfile
        file = Tempfile.new(self.name)
        file.binmode
        file.write content
        file.close
      else
        file = content
      end

      FileUploader.upload(file, self.path(DEFAULT_SIZE))
      if is_image?
        ImageResizer.resize(file, sizes) do |resized_file, size_name, size|
          FileUploader.upload(resized_file, self.path(size_name))
        end
      end
      self.save
    end
  end
end
