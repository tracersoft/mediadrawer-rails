require "mediadrawer/rails/engine"
require "mediadrawer/s3"
require 'jbuilder'
require "inflections"
require "sass-rails"
require "bootstrap-sass"
require "font-awesome-sass"
require 'rmagick'
require 'open-uri'
require 'kaminari'
require 'handlebars_assets'
require 'digest/md5'

module Mediadrawer
  @config = HashWithIndifferentAccess.new
  class << self
    attr_accessor :config
  end

  module Rails
  end
end
