module Mediadrawer
  module Rails
    class Engine < ::Rails::Engine
      isolate_namespace Mediadrawer

      initializer 'Initialize mediadrawer' do
        Mediadrawer.config.merge!(::Rails.application.config.mediadrawer)
      end

      def self.mounted_path
        route = ::Rails.application.routes.routes.detect do |_route|
          _route.app == self
        end
        route && route.path
      end
    end
  end
end
